#include <QCoreApplication>
#include "common/event/TeamChampionship.h"

int main(int argc, char *argv[])
{
//    QCoreApplication a(argc, argv);
//    return a.exec();
    TeamChampionship event = new TeamChampionship(null);

    Team homeTeam = new Team("ATTA Ablis premiere");
    Team guestTeam = new Team("ATTA Ablis deuxieme");
    homeTeam.addPlayer('A', new Player("Thibault", "T", new Date(Date.UTC(80,05,23,06,22,00)),
            Short.parseShort("1000"),Short.parseShort("1000"), Short.parseShort("1000"),
            780001, 7812345)
            );
    homeTeam.addPlayer('B', new Player("Franck", "F", new Date(),
            Short.parseShort("1000"),Short.parseShort("1000"), Short.parseShort("1000"),
            780001, 7812345)
            );
    homeTeam.addPlayer('C', new Player("Patrick", "P", new Date(),
            Short.parseShort("1000"),Short.parseShort("1000"), Short.parseShort("1000"),
            780001, 7812345)
            );
    homeTeam.addPlayer('D', new Player("Laurent", "L", new Date(),
            Short.parseShort("1000"),Short.parseShort("1000"), Short.parseShort("1000"),
            780001, 7812345)
            );
    homeTeam.addPlayer('E', new Player("Gregory", "G", new Date(),
            Short.parseShort("1000"),Short.parseShort("1000"), Short.parseShort("1000"),
            780001, 7812345)
            );
    homeTeam.addPlayer('F', new Player("Jean-Christophe", "JC", new Date(),
            Short.parseShort("1000"),Short.parseShort("1000"), Short.parseShort("1000"),
            780001, 7812345)
            );
    homeTeam.addPlayerForBottomDouble('E');
    homeTeam.addPlayerForBottomDouble('F');
    homeTeam.addPlayerForTopDouble('A');
    homeTeam.addPlayerForTopDouble('B');

    guestTeam.addPlayer('X', new Player("Marius", "M",
                                        new Date(),
                                        Short.parseShort("1000"),
                                        Short.parseShort("1000"),
                                        Short.parseShort("1000"),
                                        780001,
                                        7812345)
                        );
    guestTeam.addPlayer('Y', new Player("Jeremy", "J", new Date(),
            Short.parseShort("1000"),Short.parseShort("1000"), Short.parseShort("1000"),
            780001, 7812345)
            );
    guestTeam.addPlayer('Z', new Player("Benoit", "B", new Date(),
            Short.parseShort("1000"),Short.parseShort("1000"), Short.parseShort("1000"),
            780001, 7812345)
            );
    guestTeam.addPlayer('R', new Player("Laurent", "L", new Date(),
            Short.parseShort("1000"),Short.parseShort("1000"), Short.parseShort("1000"),
            780001, 7812345)
            );
    guestTeam.addPlayer('S', new Player("Rene", "R", new Date(),
            Short.parseShort("1000"),Short.parseShort("1000"), Short.parseShort("1000"),
            780001, 7812345)
            );
    guestTeam.addPlayer('T', new Player("Gilbert", "G", new Date(),
            Short.parseShort("1000"),Short.parseShort("1000"), Short.parseShort("1000"),
            780001, 7812345)
            );
    guestTeam.addPlayerForBottomDouble('S');
    guestTeam.addPlayerForBottomDouble('T');
    guestTeam.addPlayerForTopDouble('X');
    guestTeam.addPlayerForTopDouble('Y');

    event.setHomeTeam(homeTeam);
    event.setGuestTeam(guestTeam);

    event.generateMatches();
    event.flushMatchList();

    //Clock counter test
    Clock eventDuration = new Clock(ClockType.COUNTER);
    eventDuration.start();
    try {
        Thread.sleep(5000);
    } catch (InterruptedException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }
    // Timeout occurs
    Clock timeout = new Clock(ClockType.TIMEOUT);
    timeout.start();

    try {
        Thread.sleep(65000);
    } catch (InterruptedException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }

    eventDuration.endSession();
    try {
        eventDuration.join();
    } catch (InterruptedException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }

}
