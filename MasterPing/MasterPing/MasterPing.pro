#-------------------------------------------------
#
# Project created by QtCreator 2013-05-25T21:41:42
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MasterPing
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    common/Clock.cpp \
    common/Ranking.cpp \
    common/Player.cpp \
    common/Match.cpp \
    common/GameSet.cpp \
    common/Oponent.cpp \
    common/Team.cpp \
    common/event/Event.cpp \
    common/util/Enums.cpp \
    common/event/Teamchampionship.cpp \
    common/event/Tournamentgroupwithdirecteliminatory.cpp \
    common/event/Tournamentgroupwithfullranking.cpp \
    message/AcknowledgeMessage.cpp \
    common/Slave.cpp \
    message/Action.cpp \
    message/AMessage.cpp \
    message/EndMatch.cpp \
    message/StartMatch.cpp \
    message/Register.cpp \
    common/Master.cpp

HEADERS  += mainwindow.h \
    common/Clock.h \
    common/Ranking.h \
    common/Player.h \
    common/Match.h \
    common/GameSet.h \
    common/Oponent.h \
    common/utils/Enums.h \
    common/Team.h \
    common/event/Event.h \
    common/util/Enums.h \
    common/event/Teamchampionship.h \
    common/event/Tournamentgroupwithdirecteliminatory.h \
    common/event/Tournamentgroupwithfullranking.h \
    message/AcknowledgeMessage.h \
    common/Slave.h \
    message/Action.h \
    message/AMessage.h \
    message/EndMatch.h \
    message/StartMatch.h \
    message/Register.h \
    common/Master.h

FORMS    += mainwindow.ui
