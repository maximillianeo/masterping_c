#ifndef ACTION_H
#define ACTION_H

class Action : public AMessage
{
  Q_OBJECT
public:
  explicit Action(QObject *parent = 0);
  Action(Short messageId, Short tableNumber, ActionTheme messageTheme, String actionValue);

  /**
  * @return the serialversionuid
  */
  inline static long getSerialversionuid() {
    return serialVersionUID;
  }

  /**
   * @return the messageid
   */
  inline static Short getMessageid() {
    return messageId;
  }

  /**
   * @return the messageTheme
   */
  inline ActionTheme getMessageTheme() {
    return messageTheme;
  }

  /**
   * @return the actionValue
   */
  inline String getActionValue() {
    return actionValue;
  }

signals:
  
public slots:

protected:
  static Short messageId = 5;

private:
  static long serialVersionUID = 1L;
  ActionTheme messageTheme;
  QString actionValue;

};

#endif // ACTION_H
