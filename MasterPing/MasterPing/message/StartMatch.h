  ²éz&#ifndef STARTMATCH_H
#define STARTMATCH_H

class StartMatch : public AMessage
{
  Q_OBJECT
public:
  explicit StartMatch(QObject *parent = 0);
  StartMatch(Short messageId, Short tableNumber);

signals:
  
public slots:

protected:
  static Short messageId = 3;
private:
  static long serialVersionUID = 1L;

};

#endif // STARTMATCH_H
