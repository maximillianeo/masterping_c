#ifndef REGISTER_H
#define REGISTER_H

class Register: public AMessage
{
  Q_OBJECT
public:
  explicit Register(QObject *parent = 0);
  Register(Short tableNumber);
  /**
   * @return the serialversionuid
   */
  static long getSerialversionuid() {
    return serialVersionUID;
  }

  /**
   * @return the messageid
   */
  static Short getMessageid() {
    return messageId;
  }

  /**
   * @return the messageversion
   */
  static Byte getMessageversion() {
    return messageVersion;
  }

signals:
  
public slots:
protected:
  static Short messageId = 1;
private:
  static long serialVersionUID = 1L;
  static Byte messageVersion = Byte.parseByte("0001");
  
};

#endif // REGISTER_H
