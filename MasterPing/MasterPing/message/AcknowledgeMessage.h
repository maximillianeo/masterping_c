#ifndef ACKNOWLEDGEMESSAGE_H
#define ACKNOWLEDGEMESSAGE_H

class AcknowledgeMessage : public AMessage
{
  Q_OBJECT
public:
  explicit AcknowledgeMessage(QObject *parent = 0);
  AcknowledgeMessage(short tableNumber, Match matchToManage);
signals:
  
public slots:
  /**
   *
   */
    static long serialVersionUID = 1L;

  /**
   * @param messageId
   * @param messageVersion
   */
    AcknowledgeMessage(short tableNumber, Match matchToManage) {
            super(messageId, tableNumber);
            this.currentMatch = matchToManage;
    }

    /**
     * @return the serialversionuid
     */
    inline static long getSerialversionuid() {
            return serialVersionUID;
    }

    /**
     * @return the messageid
     */
    inline static Short getMessageid() {
            return messageId;
    }

    /**
     * @return the currentMatch
     */
    Match getCurrentMatch() {
            return currentMatch;
    }
protected:
    static Short messageId = 2;

private:
    Match currentMatch;
};

#endif // ACKNOWLEDGEMESSAGE_H
