#ifndef ENDMATCH_H
#define ENDMATCH_H

class EndMatch : public AMessage
{
  Q_OBJECT
public:
  explicit EndMatch(QObject *parent = 0);
  EndMatch(Short messageId, Short tableNumber);

signals:
  
public slots:

protected:
  static Short messageId = 4;
private:
  static long serialVersionUID = 1L;
};

#endif // ENDMATCH_H
