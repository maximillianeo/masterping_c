#include "AcknowledgeMessage.h"

AcknowledgeMessage::AcknowledgeMessage(QObject *parent) :
  AMessage(parent)
{
}
/**
 * @param messageId
 * @param messageVersion
 */
  public AcknowledgeMessage(short tableNumber, Match matchToManage) {
          super(messageId, tableNumber);
          this.currentMatch = matchToManage;
  }

  /**
   * @return the serialversionuid
   */
  public static long getSerialversionuid() {
          return serialVersionUID;
  }

  /**
   * @return the messageid
   */
  public static Short getMessageid() {
          return messageId;
  }

  /**
   * @return the currentMatch
   */
  public Match getCurrentMatch() {
          return currentMatch;
  }
