#include "Amessage.h"

AMessage::AMessage(QObject *parent) :
  QObject(parent)
{
}

const qint8& AMessage::get_messageId() const{
return this->messageId;
}

const QChar& AMessage::get_protocolVersion() const{
return this->protocolVersion;
}

const qint8& AMessage::get_tableNumber() const{
return this->tableNumber;
}

void AMessage::set_messageId(qint8 msgid){

}

void AMessage::set_protocolVersion(QChar protversion){

}

void AMessage::set_tableNumber(qint8 tblnumber){

}

//QDataStream& operator<<(QDataStream& qdsout, const AMessage& amessage)
//{
//  qdsout << amessage.get_protocolVersion()
//         << amessage.get_messageId()
//         << amessage.get_tableNumber() ;

//  return qdsout;
//}

//QDataStream& operator>>(QDataStream& qdsin, AMessage& amessage)
//{
//    QChar protocolVersion;
//    qint8 messageId;
//    qint8 tableNumber;

//    qdsin >> protocolVersion
//          >> messageId
//          >> tableNumber;

//    amessage.set_protocolVersion (protocolVersion);
//    amessage.set_messageId (messageId);
//    amessage.set_tableNumber (tableNumber);

//    return qdsin;
//}
