#ifndef AMESSAGE_H
#define AMESSAGE_H

#include <QObject>
#include <QChar>
#include <QDataStream>

class AMessage : public QObject
{
  Q_OBJECT
public:
  explicit AMessage(QObject *parent = 0);

  void set_protocolVersion(QChar);
  void set_messageId (qint8);
  void set_tableNumber(qint8);
  const QChar& get_protocolVersion() const;
  const qint8& get_messageId() const;
  const qint8& get_tableNumber() const;

signals:
  
public slots:

protected:
  QChar protocolVersion;
  qint8 messageId;
  qint8 tableNumber;
private:
  /**
   *
   */
  const static qint64 serialVersionUID = 1L;
};

QDataStream& operator<< (QDataStream &qdsout, const AMessage &amessage);
QDataStream& operator>> (QDataStream &qdsout, const AMessage &amessage);

#endif // AMESSAGE_H
