#ifndef PLAYER_H
#define PLAYER_H

#include <QObject>
#include <QString>
#include <QDate>

#include "common/ranking.h"

class Player : public QObject
{
  Q_OBJECT
public:
  explicit Player(QObject *parent = 0);
  const QString& getFirstName() const;
  const QString& getLastName() const;
  const QDate& getBirthday() const;
  const qint16& getClubNbr() const;
  const qint16& getLicenseNbr()const;
  const Ranking& getRanking() const;
signals:
  
public slots:

private:
  QString firstName;
  QString lastName;
  QDate birthday;
  qint16 licenseNbr;
  qint16 clubNbr;
  Ranking ranking;


  
};

#endif // PLAYER_H
