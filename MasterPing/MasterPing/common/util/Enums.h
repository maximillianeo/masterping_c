#ifndef ENUMS_H
#define ENUMS_H

#include <QObject>

class Enums : public QObject
{
  Q_OBJECT
public:
  explicit Enums(QObject *parent = 0);

  //message type between master and slave
  enum ActionTheme{
    HOME_PLAYER_SCORE,
    HOME_PLAYER_SET,
    HOME_PLAYER_TIMEOUT,
    GUEST_PLAYER_SCORE,
    GUEST_PLAYER_SET,
    GUEST_PLAYER_TIMEOUT,
    DEATH_MATCH_ACTIVE,
    NO_THEME
          };
  
signals:
  
public slots:
  
};

#endif // ENUMS_H
