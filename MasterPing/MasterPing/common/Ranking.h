#ifndef RANKING_H
#define RANKING_H

#include <QObject>
#include <QDate>

//TODO make that class serializable

class Ranking : public QObject
{
  Q_OBJECT
public:
  enum RankingCategory {
          VETERAN5,
          VETERAN4,
          VETERAN3,
          VETERAN2,
          VETERAN1,
          SENIOR,
          JUNIOR,
          CADET,
          MINIME,
          BENJAMIN,
          NO_CATEGORY
  };

  explicit Ranking(QObject *parent = 0);
  const qint64& getSerialVersionUID() const;
  const qint8& getRanking() const;
  const qint8& getStartingYearPoints() const;
  const qint8& getPhasePoints() const;
  const qint8& getRankingCategory()const;


signals:
  
public slots:

private:
  static QDate* Today;

  static const qint64 serialVersionUID = 1;
  qint8 ranking;
  qint8 startingYearPoints;
  qint8 startingPhasePoints;
  qint8 currentPoints;
  RankingCategory rankingCategory;
  
};

#endif // RANKING_H
