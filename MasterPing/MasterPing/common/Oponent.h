#ifndef OPONENT_H
#define OPONENT_H

#include <QObject>

class Oponent : public QObject
{
  Q_OBJECT
public:
  void winnedPoint();
  qint8 getCurrentSetScore();
  void useTimeOut();
  bool isTimeOutAvailable();
  void winnedSet();
  void giveService();
  bool hasService();

  explicit Oponent(QObject *parent = 0);

private:
  qint8 currentSetScore;
  bool isServer;
  bool isTimeOutUsed;
  qint8 nbOfWinnedSet;
  
signals:
  
public slots:
  
};

#endif // OPONENT_H
