#ifndef TOURNAMENTGROUPWITHDIRECTELIMINATORY_H
#define TOURNAMENTGROUPWITHDIRECTELIMINATORY_H

#include <QSettings>

#include "common/event/Event.h"

class TournamentGroupWithDirectEliminatory : public Event
{
public:
  TournamentGroupWithDirectEliminatory(QSettings properties);
  bool generateMatches();
  qint8 getNumberOfPlayersByGroup() const;

private:
  qint8 numberOfPlayersByGroup;
};

#endif // TOURNAMENTGROUPWITHDIRECTELIMINATORY_H
