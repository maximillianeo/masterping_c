#ifndef TOURNAMENTGROUPWITHFULLRANKING_H
#define TOURNAMENTGROUPWITHFULLRANKING_H

#include <QSettings>

#include "common/event/Event.h"

/**
 * @author maximillianeo
 * This class is used to manage a tournament divided
 * in groups depending on the number of players and after
 * groups matches a table with deterministic rank starts.
 * Everybody plays the same number of games to determines
 * the right order of all players
 */

class TournamentGroupWithFullRanking : public Event
{
public:
  TournamentGroupWithFullRanking(QString name, Event::CompetType type, QSettings properties);
  bool generateMatches();
};

#endif // TOURNAMENTGROUPWITHFULLRANKING_H
