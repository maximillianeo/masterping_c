#ifndef EVENT_H
#define EVENT_H

#include <QObject>
#include <QDate>
#include <QList>
#include <QSettings>
#include <QString>

#include "common/Match.h"

class Event : public QObject
{
  Q_OBJECT
public:
  enum CompetType{
    TOURNAMENT_GROUP_WITH_ELIMINATION,
    TOURNAMENT_ALL_VS_ALL,
    TEAM_CHAMPIONSHIP,
    PARIS_TEAM_CHAMPIONSHIP_EVENT
  };

  explicit Event(QObject *parent = 0);
  Event(QString name, CompetType type,QSettings properties);
  virtual bool generateMatches() = 0;
  int getNumberOfMatches() const;
  QList<Match*>& getMatches() const ;
  void setMatches(QList<Match> matches);
  QString& getName() const ;
  CompetType getType() const;
  QSettings& getProperties();

  /**
   * log to stdout the list of all match
   */
  //TODO get log service and write data to that service
  void flushMatchList()const ;

  void startEvent () ;
  void endEvent();
signals:
  
public slots:

protected:
  //TODO verify if vector is needed to be instantiated
  QList <Match> matches;
  QDate startingDate,
        endingDate;

  bool addMatchToCompetition (Match match);
private:
  QString name;
  CompetType type;
  QSettings properties;

};

#endif // EVENT_H
