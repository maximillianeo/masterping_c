#ifndef TEAMCHAMPIONSHIP_H
#define TEAMCHAMPIONSHIP_H

#include <QString>

#include "common/event/Event.h"
#include "common/Team.h"

class TeamChampionship : public Event
{
public:
  TeamChampionship(QSettings properties);
  /*
   * Matches order
   * A-X	1-1
   * D-R	4-4
   * B-Y	2-2
   * E-S	5-5
   * C-Z	3-3
   * F-T	6-6
   * B-X	2-1
   * DOUBLE bottom part DEF-RST 456-456
   * A-Z	1-3
   * E-R	5-4
   * C-Y	3-2
   * D-T	4-6
   * DOUBLE top part ABC-XYZ 123-123
   * F-S	6-5
   * B-Z	2-3
   * E-T	5-6
   * C-X	3-1
   * F-R	6-4
   * A-Y	1-2
   * D-S	4-5
   */
  virtual bool generateMatches();
  Team& getHomeTeam() const;
  void setHomeTeam(Team homeTeam);
  Team& getGuestTeam();
  void setGuestTeam(Team guestTeam);

private:
  Team homeTeam,
       guestTeam ;
  static QString EventName ;
};

#endif // TEAMCHAMPIONSHIP_H
