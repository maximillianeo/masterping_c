#ifndef MASTER_H
#define MASTER_H

#include <QObject>

class Master : public QObject
{
  Q_OBJECT
public:
  explicit Master(QObject *parent = 0);
  
signals:
  
public slots:
  
};

#endif // MASTER_H
