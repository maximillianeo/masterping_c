#ifndef MATCH_H
#define MATCH_H

#include <QObject>
#include <QString>
#include <QList>

class Clock;
class GameSet;

class Match : public QObject
{
  Q_OBJECT
public:
  explicit Match(QObject *parent = 0);
  
signals:
  
public slots:

private:
  QString homeplayerName;
  QString guestplayerName;
  qint8 tableNumber;
  QList<GameSet*> sets;
  GameSet* currentSet;
  Clock* matchTime;
  bool instantDeath;
};

#endif // MATCH_H
