#ifndef SET_H
#define SET_H

#include <QObject>

class Match;
class TimeViewAsCouple;
//TODO make class serializable

class GameSet : public QObject
{
  Q_OBJECT
public:
  enum ServiceSide {
    LEFT,
    RIGHT
  };

  const qint32& getStartTime()const;
  void setStartTime(const qint32&);
  const Match* getMatch()const;
  void changeServiceSide();
  const ServiceSide& getServiceSide()const;
  const TimeViewAsCouple* getCurrentTime() const;

  explicit GameSet(QObject *parent = 0);

signals:
  
public slots:

private:
  static const qint16 serialVersionUID = 1L;
  ServiceSide serviceSide;
  qint32 startTime;
  TimeViewAsCouple* currentTime;
  Match* match;
};

#endif // SET_H
