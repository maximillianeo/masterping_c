#ifndef TEAM_H
#define TEAM_H

#include <QObject>
#include <QChar>
#include <QVector>
#include <QMap>

#include "common/Player.h"

class Team : public QObject
{
  Q_OBJECT
public:

  explicit Team(QObject *parent = 0);
  Team (QString name);

  const QString& getName() const;
  qint8 addPlayer (QChar index, Player player);
  const Player& getPlayer (QChar index) const;
  const bool& addPlayerForTopDouble (QChar index);
  qint8 clearPlayersForTopDouble() const;
  bool& addPlayerForBottomDouble(QChar index);
  qint8 clearPlayersForBottomDouble() const;
  const QString& getPlayersNameForTopDouble() const;
  const QString& getPlayersNameForBottomDouble() const;
  
signals:
  
public slots:

private:

  QString name;
  QVector <QChar>  playersForTopDouble;
  QVector <QChar>  playersForBottomDouble;
  QMap <QChar,Player> players;

};

#endif // TEAM_H
