#include "clock.h"

Clock::Clock(ClockType type):
  endSession(false),
  type(type)
  {
    this->currentClock = new TimeViewAsCouple();
    this->currentClock->secondes = timeout?60:0;
    this->currentClock->minutes = 0;
};

Clock::TimeViewAsCouple::TimeViewAsCouple(){
  this->minutes = 0;
  this->secondes = 0;
};
