#ifndef CLOCK_H
#define CLOCK_H

#include <QThread>


//TODO implement thread
class Clock: public QThread {

public:
  enum ClockType{COUNTER,TIMEOUT};

  class TimeViewAsCouple{
    public:
      TimeViewAsCouple();
      long minutes;
      long secondes;
  };

  Clock(ClockType);
  bool isEndSession() const;
  void forceEndSession();
  void setEndSession(const bool);
  bool isTimeout() const;
  void endTimeout();
  const TimeViewAsCouple& getCurrentClock() const;
  void setCurrentClock(const TimeViewAsCouple&);

private:
  bool endSession, timeout;
  TimeViewAsCouple* currentClock;
  ClockType type;
};

#endif // CLOCK_H

